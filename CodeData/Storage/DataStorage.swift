//
//  DataStorage.swift
//  CodeData
//
//  Created by KaQu on 31/08/2019.
//  Copyright © 2019 KaQu. All rights reserved.
//

//import Foundation
//
//public enum DataStorage {
//  public static var coreDataStorageConfig: CoreDataStorageConfig {
//    get {
//      guard let config = _coreDataStorageConfig else { fatalError("Uninitialized CoreDataStorageConfig") }
//      return config
//    }
//    set(new) {
//      guard nil == _coreDataStorageConfig else { fatalError("Unable to change CoreDataStorageConfig") }
//      _coreDataStorageConfig = new
//    }
//  }
//  private static var _coreDataStorageConfig: CoreDataStorageConfig?
//  internal static var coreDataStorage: CoreDataStorage = {
//    CoreDataStorage.init(with: coreDataStorageConfig)
//  }()
//}
//
//
