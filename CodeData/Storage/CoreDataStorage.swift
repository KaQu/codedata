//
//  CoreDataStorage.swift
//  CodeData
//
//  Created by KaQu on 31/08/2019.
//  Copyright © 2019 KaQu. All rights reserved.
//

//import Foundation
//import CoreData
//
//public final class CoreDataStorage {
//  public static var sharedConfig: CoreDataStorageConfig {
//    get {
//      guard let config = _sharedConfig else { fatalError("Uninitialized CoreDataStorageConfig") }
//      return _sharedConfig
//    }
//    set(new) {
//      guard nil == _sharedConfig else { fatalError("Unable to change CoreDataStorageConfig") }
//      _sharedConfig = new
//    }
//  }
//  private static var _sharedConfig: CoreDataStorageConfig?
//  internal static var shared: CoreDataStorage = {
//    CoreDataStorage.init(with: coreDataStorageConfig)
//  }()
//  
//  private let accessQueue: DispatchQueue = .init(label: "data.storage")
//  private let config: CoreDataStorageConfig
//  internal let managedObjectContext: NSManagedObjectContext
//  
//  internal init(with config: CoreDataStorageConfig) {
//    self.config = config
//    let storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: DataModel.current.managedObjectModel)
//    do {
//      try storeCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: config.storageURL, options: nil)
//    } catch {
//      fatalError()
//    }
//    let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
//    context.persistentStoreCoordinator = storeCoordinator
//    self.managedObjectContext = context
//  }
//}
