//
//  CoreDataStorageConfig.swift
//  CodeData
//
//  Created by KaQu on 31/08/2019.
//  Copyright © 2019 KaQu. All rights reserved.
//

//import CoreData
//
//public struct CoreDataStorageConfig {
//  internal let storageURL: URL
//  internal let storeType: StoreType
//  
//  public init(storeType: StoreType, storageURL: URL) {
//    self.storeType = storeType
//    self.storageURL = storageURL
//  }
//  
//  public enum StoreType {
//    case sqlite
//    case binary
//    case inMemory
//    
//    internal var rawValue: String {
//      switch self {
//      case .sqlite: return NSSQLiteStoreType
//      case .binary: return NSBinaryStoreType
//      case .inMemory: return NSInMemoryStoreType
//      }
//    }
//  }
//}
