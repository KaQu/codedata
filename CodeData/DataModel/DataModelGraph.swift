//
//  DataModelGraph.swift
//  CodeData
//
//  Created by KaQu on 31/08/2019.
//  Copyright © 2019 KaQu. All rights reserved.
//

//import CoreData
//
//public final class DataModelGraph {
//    
//    internal let modelVersion: DataModel.Version
//    private var entityDescriptions: [NSEntityDescription] = []
//    private var managedObjectTypes: [DataEntity.Type] = []
//    
//    internal init(version: DataModel.Version) {
//        self.modelVersion = version
//    }
//    
//    internal var entityNames: [String] {
//        return entityDescriptions.map { $0.name! }
//    }
//    
//    internal func entityDescription<Entity: DataEntity>(for type: Entity.Type) -> NSEntityDescription {
//        let name = Entity.name
//        
//        for entityDescription in entityDescriptions where entityDescription.name == name {
//            return entityDescription
//        }
//        
//        let entityDescription = Entity.description(in: self)
//        entityDescriptions.append(entityDescription)
//        managedObjectTypes.append(type)
//        return entityDescription
//    }
//    
//    internal func managedObject(withEntityName entityName: String) -> NSManagedObject.Type {
//        for type in managedObjectTypes where type.name == entityName {
//            return type
//        }
//        
//        fatalError("Missing managed object type with entity name: \(entityName)")
//    }
//}
//
