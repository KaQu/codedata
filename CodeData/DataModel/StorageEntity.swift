//
//  DataEntity.swift
//  CodeData
//
//  Created by KaQu on 31/08/2019.
//  Copyright © 2019 KaQu. All rights reserved.
//

import CoreData


public protocol StorageEntity: Codable {}

public final class StorageSchema {}
public protocol CoreDataStorageEntity: StorageEntity {
  static func description(in schema: StorageSchema) -> NSEntityDescription
  //  init(from managed: NSManagedObject) throws
  //  static func encode(into managed: NSManagedObject) throws
}

public extension StorageEntity {

  static var entityName: String {
    return "\(self)"
  }

  static func description(in schema: StorageSchema) -> NSEntityDescription {
    let description: NSEntityDescription = .init()
    description.name = entityName

    let rootMirror: Mirror = .init(reflecting: self)
    rootMirror.children.forEach { child in
      guard let label = child.label else { return }
      let attribute: NSAttributeDescription = .init()
      attribute.name = label
      
      let childMirror: Mirror = .init(reflecting: child.value)
      if let _ = childMirror.subjectType as? StorageEntity {
        // TODO: allow relationship
        fatalError("Relationships not supported yet")
      } else {
        attribute.attributeType = coreDataType(for: childMirror.subjectType)
        attribute.isOptional = false
      }
      description.properties.append(attribute)
    }
    return description
  }
//  init(from managed: NSManagedObject) throws {
//
//  }
//  static func encode(into managed: NSManagedObject) throws {
//    let rootMirror: Mirror = .init(reflecting: self)
//    rootMirror.children.forEach { child in
//      guard let label = child.label else { return }
//      let childMirror: Mirror = .init(reflecting: child.value)
//      if let _ = childMirror.subjectType as? StorageEntity {
//        // TODO: allow relationship
//        fatalError("Not supported yet")
//      } else {
//        managed.setPrimitiveValue(child.value, forKey: label)
//      }
//    }
//  }
}

public final class CoreDataDecoder<Entity: CoreDataStorageEntity>: Decoder {
  
  private let managed: NSManagedObject

  public var codingPath: [CodingKey] = []
  
  public var userInfo: [CodingUserInfoKey : Any] = [:]
  
  internal init(managed: NSManagedObject) {
    self.managed = managed
  }
  
  public func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey {
    return KeyedDecodingContainer(CoreDataManagedContainer(managed: managed))
  }
  
  public func unkeyedContainer() throws -> UnkeyedDecodingContainer { fatalError("Not supported") }
  
  public func singleValueContainer() throws -> SingleValueDecodingContainer { fatalError("Not supported") }
  
  internal final class CoreDataManagedContainer<Key: CodingKey>: KeyedDecodingContainerProtocol {
    private let managed: NSManagedObject
    
    var codingPath: [CodingKey] = []
    
    var allKeys: [Key] = []
    
    internal init(managed: NSManagedObject) {
      self.managed = managed
    }
    
    func contains(_ key: Key) -> Bool {
      managed.entity.attributesByName.keys.contains(key.stringValue)
    }
    
    func decodeNil(forKey key: Key) throws -> Bool {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Bool else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool {
      fatalError()
    }
    
    func decode(_ type: String.Type, forKey key: Key) throws -> String {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? String else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Double.Type, forKey key: Key) throws -> Double {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Double else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Float.Type, forKey key: Key) throws -> Float {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Float else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Int.Type, forKey key: Key) throws -> Int {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Int else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Int8.Type, forKey key: Key) throws -> Int8 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Int8 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Int16.Type, forKey key: Key) throws -> Int16 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Int16 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Int32.Type, forKey key: Key) throws -> Int32 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Int32 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: Int64.Type, forKey key: Key) throws -> Int64 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? Int64 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: UInt.Type, forKey key: Key) throws -> UInt {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? UInt else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: UInt8.Type, forKey key: Key) throws -> UInt8 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? UInt8 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: UInt16.Type, forKey key: Key) throws -> UInt16 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? UInt16 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: UInt32.Type, forKey key: Key) throws -> UInt32 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? UInt32 else { fatalError("THROW") }
      return value
    }
    
    func decode(_ type: UInt64.Type, forKey key: Key) throws -> UInt64 {
      guard let value = managed.primitiveValue(forKey: key.stringValue) as? UInt64 else { fatalError("THROW") }
      return value
    }
    
    func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable {
      fatalError()
    }
    
    func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
      fatalError() // ???
    }
    
    func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer {
      fatalError()
    }
    
    func superDecoder() throws -> Decoder {
      fatalError()
    }
    
    func superDecoder(forKey key: Key) throws -> Decoder {
      fatalError()
    }
  }
}

public final class CoreDataEncoder: Encoder {
  private let managed: NSManagedObject

  public var codingPath: [CodingKey] = []

  public var userInfo: [CodingUserInfoKey : Any] = [:]
  
  internal init(managed: NSManagedObject) {
    self.managed = managed
  }

  public func container<Key>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key> where Key : CodingKey {
    return KeyedEncodingContainer(CoreDataManagedContainer(managed: managed))
  }

  public func unkeyedContainer() -> UnkeyedEncodingContainer {
    fatalError()
  }

  public func singleValueContainer() -> SingleValueEncodingContainer {
    fatalError()
  }
  
  internal final class CoreDataManagedContainer<Key: CodingKey>: KeyedEncodingContainerProtocol {
    
    private let managed: NSManagedObject
    
    var codingPath: [CodingKey] = []
    
    var allKeys: [Key] = []
    
    internal init(managed: NSManagedObject) {
      self.managed = managed
    }
    
    func encodeNil(forKey key: Key) throws {
      managed.setPrimitiveValue(nil, forKey: key.stringValue)
    }
    
    func encode(_ value: Bool, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: String, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Double, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Float, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Int, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Int8, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Int16, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Int32, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: Int64, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: UInt, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: UInt8, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: UInt16, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: UInt32, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode(_ value: UInt64, forKey key: Key) throws {
      managed.setPrimitiveValue(value, forKey: key.stringValue)
    }
    
    func encode<T>(_ value: T, forKey key: Key) throws where T : Encodable {
      fatalError()
    }
    
    func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: Key) -> KeyedEncodingContainer<NestedKey> where NestedKey : CodingKey {
      fatalError()
    }
    
    func nestedUnkeyedContainer(forKey key: Key) -> UnkeyedEncodingContainer {
      fatalError()
    }
    
    func superEncoder() -> Encoder {
      fatalError()
    }
    
    func superEncoder(forKey key: Key) -> Encoder {
      fatalError()
    }
  }
}

private func coreDataType(for type: Any.Type) -> NSAttributeType {
  if Int16.self == type { return .integer16AttributeType }
  else if Int32.self == type { return .integer32AttributeType }
  else if Int64.self == type { return .integer64AttributeType }
  else if Decimal.self == type { return .decimalAttributeType }
  else if Double.self == type { return .doubleAttributeType }
  else if Float.self == type { return .floatAttributeType }
  else if String.self == type { return .stringAttributeType }
  else if Bool.self == type { return .booleanAttributeType }
  else if Date.self == type { return .dateAttributeType }
  else if Data.self == type { return .binaryDataAttributeType }
//  else if .self == type { return .transformableAttributeType }
//  else if StorageEntity.self == type { return .objectIDAttributeType } // relations unsupported yet
  else { fatalError("Unsupported type") }
}

//public final class StorageProxy {
//  internal let managedObject: NSManagedObject? = nil
//  public init() {}
//}
//
//@propertyWrapper
//public struct Stored<Value: RawRepresentable, Entity: DataEntity> {
//  private let storageProxy: StorageProxy
//  private let name: String
//  public init(initialValue: Value, _ entity: Entity, name: String) {
//    self.storageProxy = entity.storageProxy
//    self.name = name
//  }
//  public var wrappedValue: Value {
//    get {
//      storageProxy.managedObject?.value(forKey: name)
//    }
//    set {
//      fatalError()
//    }
//  }
//}
